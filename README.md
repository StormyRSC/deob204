deob204
=======

A deobfuscated (and minimally renamed by my deobfuscation scripts) version of
RS Classic mudclient204, modified only to the extent that it loads and to
revert the ugly platform-dependent font loading introduced in 2004.

mudclient204 is the last version before the "retro revivial" reopenings of
2009, which improved the client beyond its historical capabilities when
it was the live game, but also introduced bugs and extremely heavy
obfuscation.
