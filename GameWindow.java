// Decompiled by Jad v1.5.8c. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

import java.awt.*;

public class GameWindow extends Frame {

	public GameWindow(GameShell gameshell, int i, int j, String s, boolean flag, boolean flag1) {
		anInt49 = 28;
		anInt44 = i;
		anInt45 = j;
		aGameShell48 = gameshell;
		if(flag1)
			anInt49 = 48;
		else
			anInt49 = 28;
		setTitle(s);
		setResizable(flag);
		show();
		toFront();
		resize(anInt44, anInt45);
		aGraphics46 = getGraphics();
	}

	public Graphics getGraphics() {
		Graphics g = super.getGraphics();
		if(anInt47 == 0)
			g.translate(0, 24);
		else
			g.translate(-5, 0);
		return g;
	}

	public boolean handleEvent(Event event) {
		if(event.id == 401)
			aGameShell48.keyDown(event, event.key);
		else
		if(event.id == 402)
			aGameShell48.keyUp(event, event.key);
		else
		if(event.id == 501)
			aGameShell48.mouseDown(event, event.x, event.y - 24);
		else
		if(event.id == 506)
			aGameShell48.mouseDrag(event, event.x, event.y - 24);
		else
		if(event.id == 502)
			aGameShell48.mouseUp(event, event.x, event.y - 24);
		else
		if(event.id == 503)
			aGameShell48.mouseMove(event, event.x, event.y - 24);
		else
		if(event.id == 201)
			aGameShell48.destroy();
		else
		if(event.id == 1001)
			aGameShell48.action(event, event.target);
		else
		if(event.id == 403)
			aGameShell48.keyDown(event, event.key);
		else
		if(event.id == 404)
			aGameShell48.keyUp(event, event.key);
		return true;
	}

	public final void paint(Graphics g) {
		aGameShell48.paint(g);
	}

	public void resize(int i, int j) {
		super.resize(i, j + anInt49);
	}

	int anInt44;
	int anInt45;
	Graphics aGraphics46;
	int anInt47;
	GameShell aGameShell48;
	int anInt49;
}
